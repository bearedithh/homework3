//Does the await keyword mean that the program will wait until the promise is
   //settled before continuing to the next line?

// yes the await keyword causes a async function to pause until a promise is fulfilled
// if the promise is rejected await will throw that rejection

async function async_add1(x) {
    let operation = "async_add1(" + x + ")";
    console.log("Begin " + operation);
    return new Promise((resolve) => {
        setTimeout(() => {
            console.log("Doing " + operation);
            resolve(x + 1);
            console.log("End " + operation);
        }, 1000);
    });
}

async function ex1() {
    var x = await async_add1(6);
    console.log(x);
    console.log("this will print last");
}

ex1();

//The function async_add1() has to be fulfilled before the value it returns and the last line will print