function add1(x) {
    let operation = "add1(" + x + ")";
    console.log("Begin " + operation);
    console.log("Doing " + operation);
    let ans = x + 1;
    console.log("End " + operation);
    return ans;
}

async function async_add1(x) {
    let operation = "async_add1(" + x + ")";
    console.log("Begin " + operation);
    return new Promise((resolve) => {
        setTimeout(() => {
            console.log("Doing " + operation);
            resolve(x + 1);
            console.log("End " + operation);
        }, 1000);
    });
}

function raise_exception(msg) {
    let operation = "raise_exception(" + msg + ")";
    console.log("Begin " + operation);
    console.log("Doing " + operation);
    throw msg;
    console.log("End " + operation);
}

async function async_raise_exception(msg) {
    let operation = "async_raise_exception(" + msg + ")";
    console.log("Begin " + operation);
    return new Promise((resolve, reject) => {
        console.log("Doing " + operation);
        throw msg;
        console.log("End " + operation);
    });
}

async function async_reject(msg) {
    let operation = "async_reject(" + msg + ")";
    console.log("Begin " + operation);
    return new Promise((resolve, reject) => {
        console.log("Doing " + operation);
        reject(msg);
        console.log("End " + operation);
    });
}

module.exports = {
    add1,
    async_add1,
    raise_exception,
    async_raise_exception,
    async_reject
};