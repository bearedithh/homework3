// Are you able to use multiple catches for async functions?

//I'm using a very similar example as we3.js 

async function async_raise_exception(msg) {
    let operation = "async_raise_exception(" + msg + ")";
    console.log("Begin " + operation);
    return new Promise((resolve, reject) => {
        console.log("Doing " + operation);
        throw msg;
        console.log("End " + operation);
    });
}
function main() {
    async_raise_exception("We know an error will occur because of the throw statement")
        .catch((e) => console.log("This will catch the error and be printed"))
        .then(() => console.log("This will also be printed because the error has been caught"))
        .catch((e) => console.log("This will not be called because the error has been handeled."))
}

main();

//there is nothing wrong with using multiple catch statements but they might not be necessary
//I found the books example where a catch was used for a wait() to give a database time to load helped me understand how this could be useful