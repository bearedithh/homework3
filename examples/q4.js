// If I executed an asynchronous method but didn’t await it would that be problem?
// This should not be a problem
// A simple function can demonstrate this

async function asyncNoAwait() {
    setTimeout(function() {                 //This will tell the event loop to do something after 500ms
        console.log('This should work!');
    }, 500);
}

asyncNoAwait();

//There is no await keyword in this function and it still works
//After some research this is a highly debated topic and although it works it is not recommended