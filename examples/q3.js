//So async can be used without await, but await can only be used with async right?
//The async_add1() function is used to demonstrate that this is not possible
async function async_add1(x) {
    let operation = "async_add1(" + x + ")";
    console.log("Begin " + operation);
    return new Promise((resolve) => {
        setTimeout(() => {
            console.log("Doing " + operation);
            resolve(x + 1);
            console.log("End " + operation);
        }, 1000);
    });
}

//when await is called in a non async funtion an error will be thrown
function ex3() {
    var x = await async_add1(6);
    console.log(x);
}

ex3();

//a syntax error is produced
//I tried using a catch statement but I don't think you can catch a syntax error in js